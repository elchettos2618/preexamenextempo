package modelo;
import java.util.ArrayList;
import java.sql.*;

public class dbUsuario extends dbManejador implements dbPersistencia{
    
    @Override
    public void insertar(Object objeto) throws Exception {

        Usuarios user = new Usuarios();
        user = (Usuarios) objeto;
        String consulta = "Insert into usuarios(nombre,correo,contraseña) values (?,?,?)";

        if (this.conectar()) {
            try {
                System.err.println("Se conecto correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // asignacion de valores a la consulta
                this.sqlConsulta.setString(1, user.getNombre());
                this.sqlConsulta.setString(2, user.getCorreo());
                this.sqlConsulta.setString(3, user.getContraseña());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al agregar el nuevo usuario" + e.getMessage());
            }
        }
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Usuarios> lista = new ArrayList<>();
        Usuarios user;
        if (this.conectar()) {
            String consulta = "Select * from usuarios";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros en el arreglo
            while (this.registros.next()) {
                user = new Usuarios();
                user.setIdUsuarios(this.registros.getInt("idusuarios"));
                user.setNombre(this.registros.getString("nombre"));
                user.setCorreo(this.registros.getString("correo"));
                user.setContraseña(this.registros.getString("contraseña"));
                lista.add(user);
            }
        }
        this.desconectar();
        return lista;
    }

  
     @Override
    public Object buscar(String userName) throws Exception{
        Usuarios user = null;
        if(this.conectar()){
            String consulta = "SELECT * FROM usuarios WHERE nombre = ?;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, userName);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                user = new Usuarios();
                user.setNombre(this.registros.getString("nombre"));
                user.setCorreo(this.registros.getString("correo"));
                user.setContraseña(this.registros.getString("contraseña"));
            }
            this.desconectar();
        }
        return user;
    }

  
     @Override
    public int numRegistros() throws Exception {
        int numRegistros = 0;
        if(this.conectar()){
            String consulta = "SELECT * FROM usuarios;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            while(this.registros.next())numRegistros++;
            this.desconectar();
        }
        return numRegistros;
    }

}