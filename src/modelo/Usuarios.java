package modelo;

public class Usuarios {
    private int idUsuarios;
    private String nombre;
    private String correo;
    private String contraseña;

    public Usuarios(int idUsuarios, String nombre, String correo, String contraseña) {
        this.idUsuarios = idUsuarios;
        this.nombre = nombre;
        this.correo = correo;
        this.contraseña = contraseña;
    }
    
    public Usuarios() {
        this.idUsuarios = 0;
        this.nombre = "";
        this.correo = "";
        this.contraseña = "";
    }

    public int getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(int idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}